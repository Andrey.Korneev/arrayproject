#include <iostream>
#include <ctime>

int main()
{
    const int N = 5;
    int my_array[N][N];

    // array filling
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            my_array[i][j] = i + j;
        }
    }

    // array out
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << my_array[i][j] << ' ';
        }
        std::cout << std::endl;
    }

    // sum out
    time_t seconds = time(NULL);
    tm timeinfo;
    localtime_s(&timeinfo, &seconds);
    int day = timeinfo.tm_mday;
    int index = day % N;
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += my_array[index][j];
    }
    std::cout << "Sum: " << sum << std::endl;

    return 0;
}